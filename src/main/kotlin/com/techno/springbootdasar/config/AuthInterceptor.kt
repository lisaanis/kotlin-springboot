package com.techno.springbootdasar.config

import com.fasterxml.jackson.databind.ObjectMapper
import com.techno.springbootdasar.domain.common.CommonVariable
import com.techno.springbootdasar.domain.common.StatusCode
import com.techno.springbootdasar.domain.dto.response.ResBaseDto
import com.techno.springbootdasar.domain.dto.response.ResDecodeJwtDto
import com.techno.springbootdasar.repository.UserRepository
import com.techno.springbootdasar.util.JWTGenerator
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.servlet.HandlerInterceptor
import java.util.UUID
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class AuthInterceptor (
    private val userRepository: UserRepository
) : HandlerInterceptor {
    override fun preHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any): Boolean {
        val tokenRequest = request.getHeader("token")
        val claim = JWTGenerator().decodeJWT(tokenRequest)
        val loggedInUser = userRepository.getByIdUser(UUID.fromString(claim.id))

        if (loggedInUser == null) {
            val body: ResBaseDto<String> = ResBaseDto(
                outStat = StatusCode.FAILED.code,
                outMess = CommonVariable.FAILED_MESSAGE,
                data = "JWT Token is incorrectly"
            )
            internalServerError(body, response)
            return false
        }
        return super.preHandle(request, response, handler)
    }

    fun internalServerError(body : ResBaseDto<String>, response: HttpServletResponse): HttpServletResponse {
        response.status = HttpStatus.FORBIDDEN.value()
        response.contentType = "application/json"
        response.writer.write(convertObjectToJson(body))

        return response
    }

    fun convertObjectToJson(dto: ResBaseDto<String>): String? {
        return ObjectMapper().writeValueAsString(dto)
    }
}