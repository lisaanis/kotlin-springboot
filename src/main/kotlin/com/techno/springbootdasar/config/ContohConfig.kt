package com.techno.springbootdasar.config

import com.techno.springbootdasar.service.LogicService
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component

@Component
class ContohConfig (
    private val logicService: LogicService
) {
    @Bean
    fun printName(){
        println("Hello my name Lisa")
    }

    @Bean
    fun printOddOrEven(){
        println("nilai 5 = ${logicService.oddOrEven(5)}")
    }
}